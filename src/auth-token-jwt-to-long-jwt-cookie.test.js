jest.mock('./api-config', () => {
	return {
		config: {
			secretKey: 'secretKey',
			sessionDuration:10000,
			email:{
				ttl: '1s'
			}
		}
	};
});

const JWT = require('jsonwebtoken');
const app = require('./auth-token-jwt-to-long-jwt-cookie');
const config = require('./api-config').config;

describe('auth-token-jwt-to-long-jwt-cookie',()=>{
	function replySpyMock_builder(spyObject){
		return (json)=> {
			spyObject.reply = json;
			spyObject.reply.redirect = (a,b,c)=>{
				spyObject.reply.redirectRes = [a,b,c];
				return spyObject.reply;
			};
			spyObject.reply.state = (a,b,c)=>{
				spyObject.reply.stateRes = [a,b,c];
				return spyObject.reply;
			};
			return spyObject.reply;
		};
	}
	it('no jwt -> logout with error message', () => {
		const tracker = {};
		app.handler({params:{}},replySpyMock_builder(tracker));
		expect(tracker.reply.text.toString()).toBe("JsonWebTokenError: jwt must be provided");
		expect(tracker.reply.stateRes[2].ttl).toBe(1);
	});
	it('good jwt -> logged', () => {
		const tracker = {};
		app.handler({params:{jwt:JWT.sign({}, config.secretKey)}},replySpyMock_builder(tracker));
		expect(tracker.reply.text.toString()).toBe("OK");
		expect(tracker.reply.stateRes[2].ttl).toBe(config.sessionDuration);
	});
	it('good jwt with reddirection -> logged and redirected', () => {
		const tracker = {};
		app.handler({params:{jwt:JWT.sign({url:'"https://logged.dashboard/"'}, config.secretKey)}},replySpyMock_builder(tracker));
		expect(tracker.reply.text.toString()).toBe("OK");
		expect(tracker.reply.redirectRes[0]).toBe("https://logged.dashboard/");
		expect(tracker.reply.stateRes[2].ttl).toBe(config.sessionDuration);
	});
	it('too old jwt -> error', () => {
		const tracker = {};
		const oldJWT = {params:{jwt:JWT.sign({}, config.secretKey,{expiresIn:'0ms'})}};
		//setTimeout(()=>{
			app.handler(oldJWT,replySpyMock_builder(tracker));
			expect(tracker.reply.text.toString()).toBe('TokenExpiredError: jwt expired');
			expect(tracker.reply.stateRes[2].ttl).toBe(1);
			//done();
		//},2);
	});
	it('untrusted jwt -> error', () => {
		const tracker = {};
		const oldJWT = {params:{jwt:JWT.sign({}, 'wrongKey')}};
		app.handler(oldJWT,replySpyMock_builder(tracker));
		expect(tracker.reply.text.toString()).toBe('JsonWebTokenError: invalid signature');
		expect(tracker.reply.stateRes[2].ttl).toBe(1);
	});
	it('used auth token -> error', () => {
		const tracker = {};
		const oldJWT = {params:{jwt:JWT.sign({}, config.secretKey)}};
		app.handler(oldJWT,replySpyMock_builder({useAndBurnToken:'it does'}));
		app.handler(oldJWT,replySpyMock_builder(tracker));
		expect(tracker.reply.text.toString()).toBe('TokenBurntError: already used jwt token');
		expect(tracker.reply.stateRes[2].ttl).toBe(1);
	});
});
