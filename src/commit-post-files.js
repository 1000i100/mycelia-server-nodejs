//const Joi = require("joi");
const {config, users} = require('./api-config');
const accessRightHandler = require('./accessRights');
accessRightHandler.init(users);
const store = require('./persist');
(async function(){
	try{await store.init(config,accessRightHandler);}
	catch (e){console.error(e);}
})();
const example2 ="";
const route = {
	method: "POST", path: '/commit/{lastKnownCommit}/files',
	config: {
		auth: 'jwt',
		description: "save and commit your sent files as {name}@email.ml if you have write access for them",
/*		notes:'<strong>Test me :</strong><ul>' +
		'<li>Preresquit : be authentified : <a href="'+example1+'">'+'/auth/jwt/{jwt}'+'</a></li>' +
		'<li>Send changed files from the last known commit: <a href="'+example2+'">'+example2+'</a></li>' +
		'<li>Verify the result: <a href="'+example2+'">'+example2+'</a></li>' +
		'</ul>',*/
		tags: ["files dependent access right", "registered only"],
		payload: {
			maxBytes: 500 * 1000 * 1000,
//			output: 'stream',
			parse: true,
//			allow: 'multipart/form-data'
		},
		validate: {
			query: {
				lastKnownCommit:"Last git commit hash known when sending changed files. (ancestor commit)",
				files: [{
					path: "",
					content:""}
				]
			}
		}
		/*validate: {
			query: Joi.object({
				fromHash: Joi.string().hex().required(),
				files: Joi.array().items( Joi.object({
						path: Joi.string().uri({relativeOnly:true}),
						content: Joi.string().base64()
					})
				)
			})
		}*/
	},

	handler: async function (request, reply) {
		let result;
		try{
			result = await store.save({
				'user':request.auth.credentials.user,
				'email':request.auth.credentials.email
			},
				request.params.lastKnownCommit,
				JSON.parse(request.payload.files)
			);
		} catch (e){
			console.log(e);
			result = e;
		}
		reply({text: result});
	}
};
module.exports.route = route;
