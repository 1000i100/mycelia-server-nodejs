const srv = require('./api-init-server');
const config = require('./api-config').config;
const glob = require("glob");

glob("./@(auth|commit)-+([^.]).js",{cwd:__dirname},(err,files)=>{
	const routes = [];
	for(let file of files) routes.push( require(file).route );
	addRoutes(routes);
});

function addRoutes(routes){
	srv.register([
		{
			register: require('hapi-cors'),
			options:{
				origins: config.allowedDomains
			}
		},
		require('hapi-auth-jwt2')
	], function () {
		srv.auth.strategy('jwt', 'jwt', require('./api-auth-strategy'));
		srv.auth.default('jwt');
		srv.route(routes);
	});
}
module.exports = srv;
