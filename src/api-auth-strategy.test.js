describe('api-auth-strategy', () => {
	it('fail if noSecretKey are provided', () => {
		jest.resetModules();
		jest.mock('./api-config', () => { return {config: {} }; });
		const willRun = ()=> require('./api-auth-strategy') ;
		expect(willRun).toThrow(new Error('secretKey required in config.yml'));
	});

	let app, spy, spyedCallback;
	beforeEach( ()=>{
		jest.resetModules();
		jest.mock('./api-config', () => { return {config: { secretKey: 'secretKey' },users:{
			admin: ['the@admin'],
			thisRight: ['me@me','the@admin'],
			thatOne: ['the@admin','me@me'],
			andThatToo: ['me@me']
		} }; });
		spy = {};
		spyedCallback = (a,b,c)=>spy.res = [a,b,c];
		app = require('./api-auth-strategy').validateFunc;
	});
	it('reject unknown users', () => {
		app({email:'unknown'},'',spyedCallback);
		expect(spy.res[1]).toBe(false);
	});
	it('give user access rights', () => {
		app({email:'me@me'},'',spyedCallback);
		expect(spy.res[1]).toBe(true);
		expect(spy.res[2].user).toBe('me');
		expect(spy.res[2].access).toEqual(['thisRight','thatOne','andThatToo']);
	});
});
