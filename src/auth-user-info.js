const tokenCookieMaker = require('./tokenCookieMaker');

const route = {
	method: "GET", path: "/auth/user-info",
	config: {
		auth: 'jwt',
		description: "answer with logged user name, email and access rights and renew his authentification token",
		notes:'<strong>Test me : </strong><a href="/auth/user-info'+'">/auth/user-info'+'</a>',
		tags: ["registered only"]
	},
	handler: function (request, reply) {
		tokenCookieMaker(reply(request.auth.credentials),request.auth.credentials);
	}
};
module.exports.route = route;
