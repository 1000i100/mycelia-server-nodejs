const app = require('./accessRights');
const fse = require('fs-extra');
const git = require('simple-git/promise');

describe('accessRights',()=>{
	const dataFolder = 'generated.sandbox.accessRights.test/';
	beforeEach( async ()=>{
		fse.removeSync(dataFolder);
		await fse.ensureFile(dataFolder+'.gitkeep');
		await git(dataFolder).init();
		await git(dataFolder).addConfig('user.email','test@test.test');
		await git(dataFolder).addConfig('user.name','test');
		await git(dataFolder).add(['.']);
		await git(dataFolder).commit('init');
	});
	afterEach( ()=>{
		fse.removeSync(dataFolder);
	});
	it('allow to commit when granted', async () => {
		expect.assertions(1);
		await app.init({admin:['test@test.test']});
		await expect( app.canCommit(dataFolder) ).resolves.toBe(true);
	});
	it('deny commit when not allowed', async () => {
		expect.assertions(1);
		await app.init({admin:['test@admin.test']});
		await expect( app.canCommit(dataFolder) ).resolves.toBe(false);
	});
});
