const cookie_delete = {
	ttl: 1,
	encoding: 'none',
	isSecure: false,
	isHttpOnly: true,
	clearInvalid: true,
	strictHeader: true,
	path: '/'
};

module.exports = (reply)=> reply.state("token", "", cookie_delete) ;