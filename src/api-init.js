const srv = require('./api-init-server');
require('./api-init-routes');
require('./api-doc');
require('./api-start');
module.exports = srv;
