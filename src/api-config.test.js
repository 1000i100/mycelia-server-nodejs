describe('api-config', () => {
	beforeEach( ()=>{
		jest.resetModules();
		delete process.env.DATA_FOLDER;
		jest.mock('js-yaml', () => { return {safeLoad: (fileContent)=> {return {filename:fileContent}} }; });
	});
	it('load example data by default', () => {
		jest.mock('fs', () => { return { readFileSync: (filePath) => filePath, existsSync: () => false }; });
		const app = require('./api-config');
		expect(app.config.filename).toBe('data.example/config-srv.yml');
		expect(app.users.filename).toBe('data.example/users.yml');
	});
	it('load data.dev if exist', () => {
		jest.mock('fs', () => { return { readFileSync: (filePath) => filePath, existsSync: () => true }; });
		const app = require('./api-config');
		expect(app.config.filename).toBe('data.dev/config-srv.yml');
	});
	it('load custom data if specified', () => {
		jest.mock('fs', () => { return { readFileSync: (filePath) => filePath, existsSync: () => false }; });
		process.env.DATA_FOLDER = 'customPath/';
		const app = require('./api-config');
		expect(app.config.filename).toBe('customPath/config-srv.yml');
	});
	//TODO: test process.argv[2] with relative path
	it('expose dataFolder in config', () => {
		jest.mock('fs', () => { return { readFileSync: (filePath) => filePath, existsSync: () => false }; });
		const app = require('./api-config');
		expect(app.config.dataFolder).toBe('data.example/');
	});
});
