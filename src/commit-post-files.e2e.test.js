//process.env.PORT = 3000;
jest.mock('./api-config', () => {
	return {
		config: {
			dataFolder: "generated.e2e.local/",
			secretKey: 'secretKey',
			sessionDuration: 10000,
			email: {
				ttl: '20s'
			},
			allowedDomains: ["http://notTested"]
		},
		users: {
			admin: ['the@adm.in']
		}
	};
});
jest.mock('emailjs', () => {
	let spyHost = {};
	return {
		setSpy: (spy) => spyHost.email = spy,
		message: {create: (json) => spyHost.email.messageCreate = json},
		server: {
			connect: (json) => {
				spyHost.email.serverConnect = json;
				const noError = false;
				return {send: (json, callback) => callback(noError, spyHost.email.send = json)};
			}
		}
	};
});

const http = require('http');
const fse = require('fs-extra');
const git = require('simple-git/promise');
const {config, users} = require('./api-config');
//const JWT = require('jsonwebtoken');

async function startSrv() {
	const srvService = await require('../index').start();
	return srvService.hapiSrv;
}
async function buildGitEnv(gitDistant, gitLocal) {
	await fse.ensureFile(gitDistant + '.gitkeep');
	await git(gitDistant).init();
	await git(gitDistant).addConfig('user.email', 'test@test.test');
	await git(gitDistant).addConfig('user.name', 'test');
	await git(gitDistant).addConfig('receive.denyCurrentBranch', 'updateInstead');
	await git(gitDistant).add(['.']);
	await git(gitDistant).commit('init');
	const firstCommit = (await git(gitDistant).revparse(['HEAD'])).trim();
	await git().clone(gitDistant, gitLocal);
	await git(gitLocal).addConfig('receive.denyCurrentBranch', 'updateInstead');
	return firstCommit;
}
async function send(onSrv, pathUrl, cookies, data) {
	return await new Promise((resolve, reject) => {
		let postData='';
		const headers = {};
		const reqObj = {'host': onSrv.info.host, 'port': onSrv.info.port, 'headers': headers, 'path': pathUrl};
		if (cookies) headers.Cookie = cookies;
		if (data) {
			reqObj.method = 'POST';
			for(let key in data) postData += key+'='+JSON.stringify(data[key])+'\n';
			headers['Content-Type'] = 'application/x-www-form-urlencoded';
			headers['Content-Length'] = Buffer.byteLength(postData);
		}
		let result = '';
		let req = http.request(reqObj,
			(response) => {
				response.setEncoding('utf8');
				response.on('data', (chunk) => result += chunk);
				response.on('end', () => resolve(result));
			}
		);
		if (data) req.write(postData);
		req.end();
	});
}

describe('e2e /commit/{lastKnownCommit}/files', () => {
	const backupConsoleLog = console.log;
	beforeEach(() => console.log = () => 'nothing');
	afterEach(() => console.log = backupConsoleLog);
	it('save files', async () => {
		const emailSpy = {};
		require('emailjs').setSpy(emailSpy);
		const gitDistant = 'generated.e2e.distant/';
		const firstCommit = await buildGitEnv(gitDistant, config.dataFolder);
		const srv = await startSrv();
		await send(srv, '/auth/email/'+users.admin[0],undefined,{post:true});
		const validToken = emailSpy.send.text.split('jwt/')[1].split('\n')[0];
		await send(
			srv,
			`/commit/${firstCommit}/files`,
			`token=${validToken}`,
			{'files':{'path/to.file':'content'}}
		);
		expect( (await fse.readFile(gitDistant+'path/to.file')).toString() ).toEqual('content');

		srv.stop();
		fse.removeSync(gitDistant);
		fse.removeSync(config.dataFolder);
	},20000);
});

