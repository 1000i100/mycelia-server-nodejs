const srv = require('./api-init-server');
const packageJson = require('../package.json');
srv.register([require('vision'), require('inert'), {
	register: require('lout'),
	options: {endpoint: '/', apiVersion:packageJson.version}
}], function (err) {});
