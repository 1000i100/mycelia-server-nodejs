const git = require('simple-git/promise');
//const fs = require('fs');

let users;
async function init(importUsers) {
	users = importUsers;
}
async function canCommit(gitFolder) {
	const userEmail = (await git(gitFolder).raw('config --get user.email'.split(' '))).trim();
	const userGroups = [];
	for (let group in users) {
		if (users[group].indexOf(userEmail) !== -1) userGroups.push(group);
	}
	if (!userGroups.length) return false;
	if (userGroups.indexOf('admin') !== -1) return true;
	// other predefined rights
	return false;
}

module.exports = {init,canCommit};
