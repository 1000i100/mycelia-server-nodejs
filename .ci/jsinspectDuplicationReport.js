/**
 * Created by elisa on 12/06/17.
 */
//------------------------------------INIT------------------------------------------------------------------------------
var exec = require("child_process").exec;

//------------------------------------COULEUR---------------------------------------------------------------------------
function badgeColor(percent) {
	let color = 'blue';
	if(percent>0) color = 'brightgreen'
	if(percent>20) color = 'green'
	if(percent>35) color = 'yellowgreen';
	if(percent>50) color = 'yellow';
	if(percent>70) color = 'orange';
	if(percent>85) color = 'red';
	return color;
}

//---------------------------MANiPULATION-REPORT-----------------------------------------------------------------------------------
function manipulationReportJsinspect(reportAsString)
{
	const dupliReport = JSON.parse(reportAsString);
	const maintainabilityScore = Math.round(parseFloat(dupliReport.statistics.percentage));
	let maintainabilityColor = badgeColor(maintainabilityScore);
	exec('wget -q -O ../STOCKAGE.svg https://img.shields.io/badge/maintainability-'+maintainabilityScore+'-'+maintainabilityColor+'.svg');
}



//---------------------------APPEL-DES-FONCTIONS------------------------------------------------------------------------
	exec('jsinspect -c jsinspectConfig', manipulationReportJsinspect);
