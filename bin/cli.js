#!/usr/bin/env node
//console.log(__dirname);
//console.log(process.cwd());
//console.log(process.argv);

process.on('unhandledRejection', (reason, p) => {
	console.error('Unhandled Rejection at: Promise', p, 'reason:', reason);
	// application specific logging, throwing an error, or other logic here
});
const config = {};
if(process.argv.length<=2) config.dataFolder = process.cwd()+'/';
else {
	process.argv.shift();
	process.argv.shift();
	while (process.argv.length>0){
		const action = process.argv.shift();
		switch (action){
			case 'dataFolder':
			case '--dataFolder':
			case '-f':
				if(process.argv.length === 0) console.error(`${action} must be followed by a path : ${action} myPath or ${action} "myPath"`);
				config.dataFolder = process.argv.shift();
				break;
			case 'port':
			case '--port':
			case '-p':
				if(process.argv.length === 0) console.error(`${action} must be followed by a port number : ${action} 80 as an example`);
				config.port = process.argv.shift();
				break;
			default:
				console.error(`Unknown ${action}`);
		}
	}
	console.log(process.argv);
}
const srvService = require('../index').start(config);
module.exports = srvService;
